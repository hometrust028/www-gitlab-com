---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2019-04-30

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 570   | 100%        |
| Based in the US                           | 341   | 59.82%      |
| Based in the UK                           | 26    | 4.56%       |
| Based in the Netherlands                  | 20    | 3.51%       |
| Based in Other Countries                  | 183   | 32.11%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 570   | 100%        |
| Men                                       | 426   | 75.00%      |
| Women                                     | 144   | 25.00%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 45    | 100%        |
| Men in Leadership                         | 34    | 75.56%      |
| Women in Leadership                       | 11    | 24.44%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 277   | 100%        |
| Men in Development                        | 232   | 83.75%      |
| Women in Development                      | 45    | 16.25%      |
| Other Gender Identities                   | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 341   | 100%        |
| Asian                                     | 24    | 7.04%       |
| Black or African American                 | 6     | 1.76%       |
| Hispanic or Latino                        | 23    | 6.74%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.29%       |
| Two or More Races                         | 14    | 4.11%       |
| White                                     | 198   | 58.06%      |
| Unreported                                | 75    | 21.99%      |

| Race/Ethnicity in Development (US Only)   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 110   | 100%        |
| Asian                                     | 10    | 9.09%       |
| Black or African American                 | 2     | 1.82%       |
| Hispanic or Latino                        | 7     | 6.36%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 7     | 6.36%       |
| White                                     | 64    | 58.18%      |
| Unreported                                | 20    | 18.18%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 36    | 100%        |
| Asian                                     | 5     | 13.89%      |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 0     | 0.00%       |
| Native Hawaiian or Other Pacific Islander | 1     | 2.78%       |
| Two or More Races                         | 1     | 2.78%       |
| White                                     | 19    | 52.78%      |
| Unreported                                | 10    | 27.78%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 229   | 100%        |
| Asian                                     | 22    | 9.61%       |
| Black or African American                 | 6     | 2.62%       |
| Hispanic or Latino                        | 10    | 4.37%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 5     | 2.18%       |
| White                                     | 114   | 49.78%      |
| Unreported                                | 72    | 31.44%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 168   | 100%        |
| Asian                                     | 16    | 9.52%       |
| Black or African American                 | 5     | 2.98%       |
| Hispanic or Latino                        | 10    | 5.95%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 4     | 2.38%       |
| White                                     | 85    | 50.60%      |
| Unreported                                | 48    | 28.57%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 9     | 100%        |
| Asian                                     | 0     | 0.00%       |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 1     | 11.11%      |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 0     | 0.00%       |
| White                                     | 3     | 33.33%      |
| Unreported                                | 5     | 55.56%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 570   | 100%        |
| 18-24                                     | 18    | 3.16%       |
| 25-29                                     | 119   | 20.88%      |
| 30-34                                     | 161   | 28.25%      |
| 35-39                                     | 112   | 19.65%      |
| 40-49                                     | 113   | 19.82%      |
| 50-59                                     | 40    | 7.02%       |
| 60+                                       | 6     | 1.05%       |
| Unreported                                | 1     | 0.18%       |
