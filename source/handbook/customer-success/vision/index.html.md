---
layout: markdown_page
title: "Customer Success Vision"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

*"Customer Success is when customers achieve their desired outcome through interactions with your company with the appropriate experience."* - Lincoln Murphy  

## High-Level Visual of Customer Journey
![GitLab Customer Journey](images/customer-journey.jpeg "GitLab Customer Journey")

## Objective  
Create a company-wide customer success approach, providing an engagement framework for the Customer Success organization and integrating related programs and operations from the GitLab operations (i.e., marketing, sales, customer success, product / engineering and support).  

## Goals   
Deliver faster time-to-value and customer-specific business outcomes with a world class customer experience, leveraging the full capabilities of the GitLab application. Increase average Total Contract Value (TCV) and Incremental Annual Contract Value (IACV) as well as Life-Time Value (LTV).  

## Scope
*  Processes, procedures, metrics and tools / systems for the Customer Success team
*  Integration of the related processes and operations for other GitLab business groups

## Deliverables

1.  Provide a process blueprint for the customer success engagement processes, including:
    * High-level customer journey (i.e., single stage and stage expansion)
    * High-level multi-year engagement summary
    * Processes, procedures and detailed flowcharts
    * Customer and GitLab-centric metrics
2.  Framework structure that will address specific differences and needs based on:
    * Customer segmentation (Large, SMB, Commercial)
    * Product differences (e.g., stage-specific, use case and/or feature playbooks)
    * Customer personas (e.g., executive sponsor, tools/infrastructure executive, development executive, etc.)
3.  Integrate processes and operations for direct customer engagement processes such as:
    * **Marketing**: messaging alignment and consistency, journey integration with marketing stages (e.g., awareness), customer advocacy, advisory boards, collateral development, digital journey (i.e., tech touch, hybrid digital/TAM engagement)
    * **Sales**: account planning and strategy, POC, success planning, IACV, renewal forecasting and execution, training and enablement
    * **Product / Engineering**: Product telemetry and data insights, UX journey map alignment, in-app adoption (e.g., product-led onboarding), application/stage/use case/feature adoption, "voice of customer" reporting, escalations and defect and enhancement requests
    * **Support**: Customer health, escalation processes
    * **Finance**: Financial metrics and targets (e.g., margins), budget and forecasting
    * **People Operations**: Job types, grades and families
    * **Information Technology**: Customer and operational dashboards, workflow management capabilities (i.e., SFDC, customer success platform), journey automation

## Current Priorities

1) Success planning (sales, post-transaction, tracking and closure)
2) Time-to-value (onboarding, first value, maturity model to full stage/user adoption, cross-stage expansion)
3) Renewal forecasting/sales alignment

## Process Framework
![Process Framework](images/process-framework.jpg "Process Framework")


